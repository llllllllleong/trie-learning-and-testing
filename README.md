# Trie - Learning and Testing

I created this repository to deepen my understanding of Trie Data Structures, Tokenizers and Parsers by
developing my own implementation and conducting tests. The knowledge gained
from this project was instrumental in my COMP2100 Group Project, where I
achieved an individual mark of 29/30.