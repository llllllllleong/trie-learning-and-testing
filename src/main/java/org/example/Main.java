package org.example;
import org.jline.reader.LineReader;
import org.jline.reader.LineReaderBuilder;
import org.jline.reader.impl.completer.StringsCompleter;
import org.jline.terminal.Terminal;
import org.jline.terminal.TerminalBuilder;

import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Main {


    public static void main(String[] args) {

        String inputQuery = "QID:31421 23 e @wdq wdqw qc: wd";
        Tokenizer tokenizer = new Tokenizer(inputQuery);
        Deque<Token> tokens = tokenizer.tokenize();
        for (Token token : tokens) {
            System.out.println(token);
        }
//        System.out.println(Integer.valueOf("4f891"));



//        inputQuery = "WhatIs9plus10";
//        tokenize(inputQuery);
//        inputQuery = "QC: Math";




        System.out.println(similarityScore("MultipleChoice".toCharArray(), "MultipleChoice".toCharArray()));
        System.out.println(similarityScore("ShortAnswer".toCharArray(), "mcq".toCharArray()));
        System.out.println(similarityScore("TrueorFalse".toCharArray(), "mcq".toCharArray()));
        System.out.println(similarityScore("FillInTheBlank".toCharArray(), "mcq".toCharArray()));






//        //I used gpt to generate: String[] baseQuestions, but it only generated 347
//        //questions before stopping.
//
//        //generateRandomizedQuestions will continue to generate questions until 2500
//        //The questions will not make sense, but thats ok for this test
//        String[] questions = generateRandomizedQuestions(2500);
//
//        //Create a new Trie data structure and add all the questions into it
//        Trie trie = new Trie();
//        for (String s : questions) trie.insert(s);
//        System.out.println("Finished inserting into dictionary. Dictonary size is: ");
//        System.out.println(trie.dictionarySize);
//
//
//
//
//        try {
//            Terminal terminal = TerminalBuilder.terminal();
//            LineReader lineReader = LineReaderBuilder.builder()
//                    .terminal(terminal)
//                    .completer(new StringsCompleter(trie.indexMap.values()))  // Using existing words for autocompletion
//                    .build();
//
//            while (true) {
//                String line = lineReader.readLine("Enter input: ");
//                if (line.equalsIgnoreCase("exit")) {
//                    break;
//                }
//                List<String> results = trie.getQuestionIndexFromSearch(line);
//                System.out.println("Autocomplete Results: ");
//                questionListToString(results);
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }








//        // Get the Java runtime
//        Runtime runtime = Runtime.getRuntime();
//        // Run the garbage collector
//        runtime.gc();
//        // Calculate the used memory
//        long memory = runtime.totalMemory() - runtime.freeMemory();
//        System.out.println("Used memory is bytes: " + memory);
//        System.out.println("Used memory is megabytes: "
//                + bytesToMegabytes(memory));
    }


    public static int similarityScore(char[] a, char[] b) {
        int[][] dpArray = new int[a.length + 1][b.length + 1];
        for (int i = 0; i <= a.length; i++) {
            for (int j = 0; j <= b.length; j++) {
                if (i == 0) {
                    dpArray[i][j] = j;
                }
                else if (j == 0) {
                    dpArray[i][j] = i;
                }
                else {
                    int substitutionCost = (a[i-1] == b[j-1] ? 0 : 1);
                    int substitution = dpArray[i-1][j-1] + substitutionCost;
                    int insertion = dpArray[i-1][j] + 1;
                    int deletion = dpArray[i][j-1] + 1;
                    dpArray[i][j] = Math.min(substitution, Math.min(insertion, deletion));
                }
            }
        }
        return dpArray[a.length][b.length];
    }




    private static final long MEGABYTE = 1024L * 1024L;

    public static long bytesToMegabytes(long bytes) {
        return bytes / MEGABYTE;
    }
    public static String[] generateRandomizedQuestions(int totalQuestions) {
        //GPT generated questions of max length == 11 words
        Random random = new Random();
        String[] newQuestions = new String[totalQuestions];
        HashMap<Integer, List<String>> hm = new HashMap<>();
        for (int i = 1; i < 12; i++) {
            hm.put(i, new ArrayList<>());
        }
        for (String question : baseQuestions) {
            int i = 1;
            String[] split = question.split(" ");
            for (String word : split) {
                hm.get(i).add(word);
                i++;
            }
        }
        for (int q = 0; q < 347; q++) {
            newQuestions[q] = baseQuestions[q];
        }
        for (int q = 347; q < totalQuestions; q++) {
            // Randomly decide how many words the new question should have
            int wordsCount = random.nextInt(5) + 7; // Generates questions of length 7 to 11 words
            String currentQuestion = "";
            for (int i = 1; i <= wordsCount; i++) {
                List<String> ithWord = hm.get(i);
                currentQuestion += ithWord.get(random.nextInt(ithWord.size())) + " ";
            }
            currentQuestion = currentQuestion.substring(0,currentQuestion.length()-1);
            newQuestions[q] = currentQuestion;
        }
        return newQuestions;
    }


    //Generated from GPT
    //347 generations before it stopped
    //Max length of a question is 11 words
    public static String[] baseQuestions = {
            "Find the eigenvalues of a 3x3 matrix",
            "Calculate the determinant of a 4x4 matrix",
            "Solve for x in a linear equation",
            "Find the area under a curve using integration",
            "Determine the derivative of a polynomial function",
            "Calculate the cross product of two vectors",
            "Find the eigenvectors of a 3x3 matrix",
            "Evaluate the integral of a trigonometric function",
            "Solve for the limit as x approaches infinity",
            "Determine the roots of a quadratic equation",
            "Calculate the dot product of two vectors",
            "Prove a given mathematical theorem",
            "Solve a system of linear equations using matrices",
            "Determine the mean and standard deviation of a dataset",
            "Calculate the volume of a solid of revolution",
            "Find the Laplace transform of a function",
            "Determine the Taylor series for a function around a point",
            "Find the roots of a cubic equation",
            "Calculate the surface area of a cylinder",
            "Determine the eigenvalue decomposition of a 4x4 matrix",
            "Solve for the inverse of a 3x3 matrix",
            "Determine if a set is closed under addition",
            "Find the eigenvalues and eigenvectors of a symmetric matrix",
            "Calculate the Fourier transform of a periodic function",
            "Find the rank of a matrix",
            "Determine if a set of vectors is linearly independent",
            "Solve a linear recurrence relation",
            "Find the points of intersection between two curves",
            "Determine if a function is injective",
            "Calculate the roots of a quartic equation",
            "Find the eigenvectors of a Hermitian matrix",
            "Determine the points of inflection on a curve",
            "Calculate the skewness and kurtosis of a distribution",
            "Determine if a function has removable discontinuities",
            "Solve a system of differential equations using eigenvalues",
            "Find the eigenvalues of a complex symmetric matrix",
            "Calculate the spectrum of a non-diagonalizable matrix",
            "Determine if a series converges or diverges",
            "Calculate the gradient of a multivariable function",
            "Find the Jacobian matrix for a set of functions",
            "Solve a system of non-linear equations",
            "Determine if a function is monotonic",
            "Calculate the Fourier coefficients of a periodic function",
            "Find the eigenvectors of a complex Hermitian matrix",
            "Determine the order of a permutation",
            "Solve a linear programming problem",
            "Calculate the arc length of a curve",
            "Determine the divergence and curl of a vector field",
            "Find the eigenvalue decomposition of a sparse matrix",
            "Calculate the trace of a 3x3 matrix",
            "Determine if a function is surjective",
            "Calculate the binomial coefficient",
            "Find the orthogonal projection of a vector",
            "Determine if two events are independent",
            "Find the partial derivative of a multivariable function",
            "Calculate the rank-nullity theorem",
            "Determine the minimal polynomial for a given matrix",
            "Solve a system of non-homogeneous differential equations",
            "Find the eigenvectors of a real skew-symmetric matrix",
            "Calculate the orthogonal projection of a function",
            "Determine the roots of a quartic polynomial",
            "Solve a linear regression problem",
            "Find the eigenvalues of a symmetric banded matrix",
            "Determine if a function is even or odd",
            "Calculate the Fourier transform of a piecewise function",
            "Determine if a matrix is singular",
            "Find the cross product of two parametric functions",
            "Calculate the volume of a parallelepiped",
            "Find the eigenvectors of a skew-symmetric matrix",
            "Solve a system of linear differential equations",
            "Determine if a set is convex",
            "Calculate the orthogonal basis for a set of vectors",
            "Find the arc length of a space curve",
            "Determine if a function has a vertical asymptote",
            "Calculate the harmonic mean of a dataset",
            "Find the eigenvalues of a skew-symmetric banded matrix",
            "Evaluate a double integral over a polar region",
            "Calculate the Fourier coefficients of a periodic function",
            "Determine if a function is differentiable at a given point",
            "Find the eigenvalues of a complex diagonalizable matrix",
            "Determine if two functions are inverses",
            "Calculate the orthogonal projection of a parametric function",
            "Solve for the eigenvalues of a symmetric matrix",
            "Determine if a function has a horizontal asymptote",
            "Calculate the skewness and kurtosis of a distribution",
            "Find the eigenvectors of a complex Hermitian matrix",
            "Calculate the trace of a triangular matrix",
            "Determine if two matrices are block-diagonal",
            "Calculate the surface area of a regular cylinder",
            "Find the eigenvalues and eigenvectors of a complex symmetric banded matrix",
            "Evaluate the integral of a rational function",
            "Determine if a series converges absolutely",
            "Calculate the Fourier transform of a parametric function",
            "Find the eigenvectors of a real skew-symmetric matrix",
            "Solve for the derivatives of a multivariable function",
            "Calculate the spectrum of a complex Hermitian matrix",
            "Determine if a set has closure under addition",
            "Find the eigenvalues of a complex symmetric banded matrix",
            "Calculate the orthogonal basis for a set of parametric functions",
            "Determine if a matrix is lower triangular",
            "Solve for the derivatives of a parametric function",
            "Calculate the Fourier coefficients for a piecewise linear function",
            "Find the eigenvectors of a singular matrix",
            "Determine the spectrum of a skew-symmetric matrix",
            "Calculate the surface area of a frustum",
            "Find the eigenvalues and eigenvectors of a complex symmetric matrix",
            "Determine if a function has removable discontinuities",
            "Calculate the rank of a sparse matrix",
            "Evaluate a triple integral over a cylindrical region",
            "Determine if a set has closure under multiplication",
            "Calculate the Fourier transform of a piecewise function",
            "Solve for the eigenvalues of a sparse matrix",
            "Calculate the volume of a tetrahedron",
            "Find the eigenvectors of a complex Hermitian banded matrix",
            "Determine the eigenvalue decomposition of a block-diagonal matrix",
            "Calculate the trace of a block-diagonal matrix",
            "Determine if a set is closed under addition",
            "Find the orthogonal projection of a set of vectors",
            "Calculate the Fourier transform of a parametric function",
            "Determine if two functions are orthogonal",
            "Calculate the Fourier coefficients of a periodic function",
            "Find the eigenvalues of a complex Hermitian matrix",
            "Determine if two matrices are isomorphic",
            "Calculate the orthogonal projection of a parametric function",
            "Find the eigenvalues of a skew-symmetric banded matrix",
            "Calculate the Fourier coefficients for a complex function",
            "Determine if a series converges conditionally",
            "Evaluate a triple integral over a spherical region",
            "Find the eigenvalues of a complex Hermitian banded matrix",
            "Determine if a set has closure under multiplication",
            "Calculate the Fourier transform for a piecewise linear function",
            "Find the eigenvectors of a real skew-symmetric matrix",
            "Calculate the Fourier transform of a periodic function",
            "Solve for the critical points of a function",
            "Determine if a series converges absolutely",
            "Find the eigenvalues of a skew-symmetric banded matrix",
            "Evaluate the triple integral over a polar region",
            "Determine if two sets are equivalent",
            "Calculate the orthogonal projection of a parametric function",
            "Determine if two matrices are isomorphic",
            "Calculate the surface area of a regular prism",
            "Find the eigenvalues and eigenvectors of a complex symmetric banded matrix",
            "Calculate the spectrum of a Hermitian matrix",
            "Find the derivatives of a parametric function",
            "Calculate the Fourier transform for a piecewise function",
            "Determine if two functions are linearly independent",
            "Solve for the derivatives of a parametric function",
            "Calculate the Fourier transform of a piecewise linear function",
            "Find the eigenvalues of a complex symmetric banded matrix",
            "Determine if a set is closed under addition",
            "Find the orthogonal basis for a set of parametric functions",
            "Calculate the Fourier coefficients of a complex function",
            "Determine the eigenvalues of a non-diagonalizable matrix",
            "Find the Fourier series of a periodic function",
            "Solve a system of linear differential equations",
            "Determine the Fourier transform of a periodic function",
            "Calculate the Fourier transform for a piecewise linear function",
            "Find the eigenvectors of a complex symmetric matrix",
            "Determine if a set has closure under multiplication",
            "Find the eigenvalues of a complex Hermitian banded matrix",
            "Calculate the Fourier transform of a piecewise function",
            "Determine if a series converges absolutely",
            "Evaluate a double integral over a spherical region",
            "Find the eigenvectors of a real skew-symmetric matrix",
            "Calculate the spectrum of a skew-symmetric matrix",
            "Determine if a set is convex",
            "Calculate the Fourier transform of a periodic function",
            "Find the eigenvalues of a complex symmetric matrix",
            "Determine if a series is conditionally convergent",
            "Calculate the Fourier transform of a piecewise linear function",
            "Solve a system of differential equations using eigenvalues",
            "Calculate the orthogonal projection of a parametric function",
            "Find the derivatives of a higher-order polynomial",
            "Determine if two matrices are block-diagonal",
            "Calculate the Fourier coefficients of a periodic function",
            "Find the eigenvalues of a complex diagonalizable matrix",
            "Determine if a function has a horizontal asymptote",
            "Calculate the Fourier transform of a parametric function",
            "Find the eigenvectors of a real skew-symmetric matrix",
            "Determine if a series converges or diverges",
            "Calculate the Fourier coefficients of a piecewise linear function",
            "Solve a system of differential equations",
            "Find the eigenvectors of a skew-symmetric matrix",
            "Determine if two matrices are equivalent",
            "Calculate the surface area of a regular prism",
            "Find the eigenvalues of a skew-symmetric banded matrix",
            "Determine if a function is surjective",
            "Calculate the Fourier transform of a piecewise linear function",
            "Evaluate a triple integral over a polar region",
            "Determine if two functions are orthogonal",
            "Calculate the Fourier transform of a periodic function",
            "Find the eigenvalues of a complex symmetric banded matrix",
            "Solve a system of non-linear equations",
            "Determine if two functions are inverses",
            "Calculate the Fourier coefficients for a complex function",
            "Find the orthogonal basis for a set of parametric functions",
            "Determine if a function has a horizontal asymptote",
            "Solve a system of linear differential equations",
            "Calculate the Fourier transform of a piecewise function",
            "Determine if a set has closure under addition",
            "Find the orthogonal basis for a set of vectors",
            "Calculate the surface area of a frustum",
            "Find the eigenvalues of a complex diagonalizable matrix",
            "Determine the eigenvalue decomposition of a block-diagonal matrix",
            "Solve a system of non-linear equations",
            "Calculate the spectrum of a skew-symmetric matrix",
            "Find the eigenvectors of a real skew-symmetric matrix",
            "Determine if a series converges or diverges",
            "Calculate the Fourier transform of a periodic function",
            "Find the eigenvalues and eigenvectors of a complex symmetric matrix",
            "Solve a system of differential equations",
            "Determine if a set has closure under multiplication",
            "Calculate the Fourier transform of a piecewise linear function",
            "Find the eigenvectors of a complex Hermitian banded matrix",
            "Determine if two functions are orthogonal",
            "Calculate the Fourier transform of a piecewise function",
            "Determine if two matrices are equivalent",
            "Calculate the surface area of a cone",
            "Find the eigenvalues of a skew-symmetric banded matrix",
            "Determine if two matrices are isomorphic",
            "Calculate the Fourier transform for a piecewise function",
            "Find the orthogonal projection of a parametric function",
            "Calculate the trace of a block-diagonal matrix",
            "Solve a system of differential equations",
            "Determine if two matrices are block-diagonal",
            "Calculate the Fourier transform of a piecewise linear function",
            "Find the eigenvalues and eigenvectors of a complex symmetric banded matrix",
            "Calculate the Fourier coefficients for a complex function",
            "Determine if two functions are inverses",
            "Solve a system of non-linear equations",
            "Determine if a function has removable discontinuities",
            "Calculate the Fourier transform for a periodic function",
            "Determine if two sets are equivalent",
            "Calculate the Fourier coefficients for a complex function",
            "Evaluate a triple integral over a spherical region",
            "Find the eigenvectors of a complex Hermitian matrix",
            "Determine if a set has closure under multiplication",
            "Solve for the derivatives of a parametric function",
            "Calculate the Fourier coefficients for a periodic function",
            "Find the eigenvectors of a real skew-symmetric matrix",
            "Determine if a set is convex",
            "Evaluate a triple integral over a cylindrical region",
            "Find the derivatives of a higher-order polynomial",
            "Determine if two sets are disjoint",
            "Calculate the surface area of a regular cylinder",
            "Find the eigenvalues of a complex Hermitian matrix",
            "Determine if a set has closure under addition",
            "Calculate the Fourier transform of a piecewise linear function",
            "Determine if a function has a vertical asymptote",
            "Calculate the trace of a triangular matrix",
            "Find the eigenvectors of a complex Hermitian banded matrix",
            "Solve a system of linear differential equations",
            "Determine if two matrices are equivalent",
            "Calculate the Fourier transform of a piecewise function",
            "Find the eigenvalues of a complex Hermitian matrix",
            "Calculate the Fourier transform for a piecewise linear function",
            "Determine if a function is surjective",
            "Calculate the rank-nullity theorem",
            "Find the eigenvalues of a skew-symmetric banded matrix",
            "Determine if two matrices are isomorphic",
            "Calculate the Fourier coefficients for a complex function",
            "Solve a system of differential equations using eigenvalues",
            "Calculate the Fourier coefficients for a periodic function",
            "Find the eigenvectors of a real skew-symmetric matrix",
            "Determine if a function has a horizontal asymptote",
            "Calculate the Fourier transform of a piecewise linear function",
            "Solve for the eigenvalues of a sparse matrix",
            "Determine if a set has closure under addition",
            "Calculate the spectrum of a non-diagonalizable matrix",
            "Find the eigenvectors of a complex Hermitian matrix",
            "Determine the orthogonal basis for a set of vectors",
            "Calculate the Fourier coefficients for a complex function",
            "Solve a system of linear differential equations",
            "Determine if two matrices are equivalent",
            "Calculate the Fourier transform of a piecewise linear function",
            "Find the eigenvalues of a complex skew-symmetric matrix",
            "Determine if two functions are inverses",
            "Calculate the Fourier coefficients for a complex function",
            "Determine if a set is convex",
            "Find the eigenvalues of a real skew-symmetric matrix",
            "Calculate the Fourier coefficients of a periodic function",
            "Solve a system of linear differential equations",
            "Determine if a set is closed under addition",
            "Find the eigenvectors of a skew-symmetric banded matrix",
            "Calculate the Fourier coefficients of a periodic function",
            "Evaluate a triple integral over a cylindrical region",
            "Determine if two sets are equivalent",
            "Calculate the surface area of a regular prism",
            "Find the derivatives of a higher-order polynomial",
            "Determine if two matrices are equivalent",
            "Calculate the Fourier transform for a piecewise linear function",
            "Determine if two functions are inverses",
            "Calculate the orthogonal basis for a set of vectors",
            "Evaluate a triple integral over a polar region",
            "Find the eigenvalues of a complex Hermitian banded matrix",
            "Determine if two functions are inverses",
            "Calculate the Fourier coefficients of a periodic function",
            "Solve a system of non-linear equations",
            "Find the orthogonal basis for a set of parametric functions",
            "Determine if a series converges conditionally",
            "Calculate the spectrum of a non-diagonalizable matrix",
            "Find the derivatives of a multivariable function",
            "Solve a system of differential equations",
            "Determine if a function has a horizontal asymptote",
            "Calculate the Fourier transform of a piecewise function",
            "Determine if two matrices are block-diagonal",
            "Calculate the Fourier coefficients for a periodic function",
            "Find the eigenvectors of a real skew-symmetric matrix",
            "Determine if two functions are inverses",
            "Calculate the Fourier transform of a periodic function",
            "Solve a system of linear differential equations",
            "Determine if two matrices are isomorphic",
            "Calculate the Fourier coefficients of a complex function",
            "Find the derivatives of a multivariable function",
            "Evaluate the triple integral over a polar region",
            "Determine if two sets are equivalent",
            "Calculate the Fourier coefficients of a piecewise linear function",
            "Find the eigenvectors of a skew-symmetric banded matrix",
            "Calculate the spectrum of a skew-symmetric matrix",
            "Determine if two functions are orthogonal",
            "Calculate the Fourier coefficients for a piecewise linear function",
            "Determine if two matrices are equivalent",
            "Calculate the Fourier transform for a periodic function",
            "Find the derivatives of a multivariable function",
            "Determine if two matrices are isomorphic",
            "Calculate the Fourier coefficients for a complex function",
            "Solve a system of linear differential equations",
            "Determine if a set is closed under addition",
            "Find the eigenvectors of a real skew-symmetric matrix",
            "Determine the Fourier transform for a complex function",
            "Calculate the trace of a block-diagonal matrix",
            "Evaluate the integral of a piecewise function",
            "Find the eigenvectors of a complex Hermitian banded matrix",
            "Calculate the Fourier transform for a complex function",
            "Determine if a function has removable discontinuities",
            "Find the eigenvalues of a complex Hermitian matrix",
            "Determine if two functions are inverses",
            "Calculate the Fourier coefficients of a periodic function",
            "Solve for the derivatives of a parametric function",
            "Calculate the Fourier transform of a periodic function",
            "Determine if a set has closure under multiplication",
            "Find the orthogonal basis for a set of vectors",
            "Calculate the surface area of a regular prism",
            "Determine if a series converges conditionally",
            "Find the eigenvalues of a complex Hermitian banded matrix",
            "Calculate the Fourier coefficients for a periodic function",
            "Solve a system of differential equations",
    };

}