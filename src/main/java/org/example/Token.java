package org.example;

import java.util.Objects;

/**
 * Implementation of a search query token.
 * @author Leong Yin Chan
 */
public class Token {
    /**
     * Enum to define the possible types of tokens that can be identified in the search queries.
     */
    public enum Type {
        UserQuantifier,
        CategoryQuantifier,
        TypeQuantifier,
        IDQuantifier,
        Number,
        Word
    }

    private Type type;
    private String tokenString;
    /**
     * Constructs a new Token with specified type and content.
     *
     * @param type The type of the token, as defined in the Type enum.
     * @param tokenString The string content of the token.
     */

    public Token(Type type, String tokenString) {
        this.type = type;
        this.tokenString = tokenString;
    }
    /**
     * Returns the type of the token.
     *
     * @return The type of the token.
     */
    public Type getType() {
        return type;
    }

    /**
     * Returns the string value of the token.
     *
     * @return The string content of the token.
     */
    public String getTokenString() {
        return tokenString;
    }

    /**
     * Returns a string representation of the token, including its type and content.
     *
     * @return A string representing the token.
     */
    @Override
    public String toString() {
        return type + " Token: (" + tokenString + ")";
    }
}