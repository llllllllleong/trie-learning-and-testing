package org.example;


import java.util.ArrayDeque;
import java.util.Deque;
import java.util.regex.Matcher;
import java.util.regex.Pattern;




public class Tokenizer {
    private String inputString;
    private Deque<Token> tokenQueue;

    public Tokenizer(String input) {
        this.inputString = input.trim();
        this.tokenQueue = new ArrayDeque<>();
    }

    public Deque<Token> tokenize() {
        if (inputString == null || inputString.isEmpty()) return tokenQueue;

        String patternString = "(?i)(@|QC:|QT:|QID:|\\d+|\\w+)";
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(inputString);

        while (matcher.find()) {
            String match = matcher.group();

            if (match.matches("@")) {
                tokenQueue.add(new Token(Token.Type.UserQuantifier, match));
            } else if (match.toLowerCase().matches("qc:")) {
                tokenQueue.add(new Token(Token.Type.CategoryQuantifier, match));
            } else if (match.toLowerCase().matches("qt:")) {
                tokenQueue.add(new Token(Token.Type.TypeQuantifier, match));
            } else if (match.toLowerCase().matches("qid:")) {
                tokenQueue.add(new Token(Token.Type.IDQuantifier, match));
            } else if (match.matches("\\d+")) {
                tokenQueue.add(new Token(Token.Type.Number, match));
            } else if (match.matches("\\w+")) {
                tokenQueue.add(new Token(Token.Type.Word, match));
            }
        }

        return tokenQueue;
    }
}