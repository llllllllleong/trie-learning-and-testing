package org.example;


import java.util.*;

class Trie {
    Node root;
    HashMap<Integer, String> indexMap;

    int dictionarySize = 0;

    public Trie() {
        root = new Node();
        indexMap = new HashMap<>();
    }

    public void insert(String word) {
        dictionarySize++;
        indexMap.put(dictionarySize, word);
        word = word.toLowerCase();
        word = word.replaceAll("[^a-z]", "");
        root.insert(word,0, dictionarySize);
    }

    public boolean search(String word) {
        return root.search(word, 0);
    }

    public List<String> getQuestionIndexFromSearch(String s) {
        s = s.toLowerCase();
        s = s.replaceAll("[^a-z]", "");
        List<Integer> integerList =  root.getQuestionIndexFromSearch(s,0);
        List<String> output = new ArrayList<>();
        for (Integer I : integerList) {
            output.add(indexMap.get(I));
        }
        return output;
    }

    class Node {
        Node[] children;
        boolean eow;
        List<Integer> questionIndex;

        public Node() {
            children = new Node[26];
        }

        public void insert(String s, int index, int sIndexInMap) {
            if (index == s.length()) {
                return;
            } else {
                char c = s.charAt(index);
                int cIndex = c - 'a';
                if (children[cIndex] == null) {
                    children[cIndex] = new Node();
                }
                children[cIndex].insert(s, index + 1, sIndexInMap);
                if (index == s.length() - 1) {
                    children[cIndex].eow = true;
                    if (children[cIndex].questionIndex == null) children[cIndex].questionIndex = new ArrayList<>();
                    children[cIndex].questionIndex.add(sIndexInMap);
                }
            }
        }

        public boolean search(String s, int index) {
            if (index == s.length()) return false;
            char c = s.charAt(index);
            int cIndex = c - 'a';
            Node n = children[cIndex];
            if (n == null) return false;
            if (index == s.length() - 1) return n.eow;
            return n.search(s, index + 1);
        }

        public List<Integer> autoFill() {
            List<Integer> output = new ArrayList<>();
            if (eow) output = questionIndex;
            for (Node n : children) {
                if (output.size() >= 5) break;
                if (n == null) continue;
                List<Integer> nextAutoFill = n.autoFill();
                for (Integer I : nextAutoFill) {
                    if (output.size() >= 5) break;
                    output.add(I);
                }
            }
            return output;
        }


        public List<Integer> getQuestionIndexFromSearch(String s, int index) {
            List<Integer> output = new ArrayList<>();
            if (index < s.length()) {
                char c = s.charAt(index);
                int cIndex = c - 'a';
                Node n = children[cIndex];
                if (n == null) return output;
                return n.getQuestionIndexFromSearch(s, index + 1);
            } else {
                return autoFill();
            }
        }



    }
}

